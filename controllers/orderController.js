const Order = require("../models/Order");
const auth = require("../auth");
const User = require("../models/User");
const Product = require("../models/Product");


// Controllers to place an order

module.exports.makeOrder = (data) => {

	console.log(data);

	let sum = 0;
	let i = 0;

	for(i; i< data.order.products.length; i++){

		sum = sum + data.order.products[i].quantity * data.order.products[i].price;
	};


	if(!(data.isAdmin)){
		let newOrder = new Order({

			userId: data.order.userId,
			products: data.order.products,
			totalAmount: sum

		});

		return newOrder.save().then((order,err) => {

			if(err){

				// return "Error Occurred!"
				return false;
			} else{

				return true;
				// return "Order Successful!"
			};
		});
	};

	let message = Promise.resolve("User must be a non-admin for to access this!");
	return message.then((value) => {
		return value;
	});

};

// Controllers to retrieve all orders (Admin only)

module.exports.getAllOrders = (isAdmin) => {

	if(isAdmin){

		return Order.find({}).then(result => {
				return result;
		});
	};
};



// Controllers to retrieve all orders of a particular orders

module.exports.getMyOrders = (reqParams) => {

		return Order.find({ userId: reqParams.userId}).then(result => {

			return result;
		});
};

























































