const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require("../auth");


// Controllers for checking if Email is already registered

module.exports.checkEmailExists = (reqBody) => {
		return User.find({ email : reqBody.email }).then(result => {
			// The "find" method returns a recod if a match is found

				if(result.length > 0){

					return true;

				} else{

					return false;
				}
		});
}

// Controllers for registrating user

module.exports.registerUser = (reqBody) => {
	

	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
		// here using of 10 means hashing is done 10 times.
	})

	// the .save() callback function returns user and error
	return newUser.save().then((user, err)=> {

			if(err){
				return false
			} else {
				return true
			}
	});

}

// Controller for user login

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		if(result == null){

			return false;
		} else{

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return{ access: auth.createAccessToken(result), id: result._id}
			} else{

				return false;
			};
		};
	});
};


// Controller to retrieve user details

module.exports.getDetails = (reqParams) => {

	// console.log(reqParams)
	return User.findOne({_id: reqParams.userId}).then(result => {

		return result;
	})
	.catch(error => {
    console.error('Error:', error);
	});
};

// Controllers to make an user as an Admin

module.exports.makeAdmin = (data) => {

	console.log(data);
	if(data.isAdmin){
		let userToAdmin = {

			isAdmin: true
		}

		return User.findByIdAndUpdate( data.userId, userToAdmin).then((user,err) => {

				if(err){

					return "Error Occurred!";
				} else{

					return "You are an Admin now!"
				};	
		});
	};
	
};







































