const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	userId: {
		type: String, 
		required: [ true, " UserId is required!"]
	},

	products: [
		{	
			name: {
				type: String,
				required: [ true, "Name of product is required!"]
			},

			productId: {
				type: String,
				required: [ true, "PoductId is required!"]
			},

			price: {
				type: Number,
				required: [true, "Price of product is required"]
			},

			quantity: {
				type: Number,
				required: [true, "Product quantity is required!"]
			}
		}
	],

	totalAmount: {
		type: Number,
		required: [true, "Total amount is required!"]
	},

	purchasedOn:{
		type: Date,
		default: new Date()
	}
	
})

module.exports = mongoose.model("Order", orderSchema);







































