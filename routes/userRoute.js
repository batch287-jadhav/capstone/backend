const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/userController");


// Route to check if the Email is already registered

router.post("/checkEmail", (req,res) => {

	userController.checkEmailExists(req.body).then(resultFromController => {
		
		res.send(resultFromController)
	});
});

// Creating route for user registration 

router.post("/register", (req,res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Creating route for user authentification

router.post("/login", (req,res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Creating route for getting user details

router.get("/:userId/userDetails", auth.verify, (req,res) => {

	userController.getDetails(req.params).then(resultFromController => res.send(resultFromController));
});


// Route for setting an user as an Admin

router.put("/:userId/setAsAdmin", auth.verify, (req,res) => {

	console.log(req.params);
	let data = {
		userId: req.body.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.makeAdmin(data).then(resultFromController => res.send(resultFromController));

});





module.exports = router;












































